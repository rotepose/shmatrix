/*
|
| Copyright: Jeremy Cartwright (C) 2021
|   License: BSD 3 Clause License
|
|    Author: Jeremy Cartwright
|   Contact: jeremy@rotepose.com
|   Created: 24-FEB-2021
|    matrix: an attempt at the matrix screensaver in terminal
|
|            in .tmux.conf place these lines to enable your screensaver
|            set -g lock-command shmatrix
|            set -g lock-after-time 180
|            bind L lock-session # optional: to lock on demand
|
*/

#include "shmatrix.h"

// take the red pill
int
main(void) {
/*{{{*/
	
	rain(getwin());

	return 0;
	
}/*}}}*/

// get size of current window
struct scrn
getwin(void) {
/*{{{*/
	struct winsize w;
	struct scrn s;

	ioctl(STDIN_FILENO, TIOCGWINSZ, &w);
	s.r = w.ws_row;
	s.c = w.ws_col;

	return s;
	
}/*}}}*/

int
rain(struct scrn s) {
/*{{{*/
	int drops[s.c];
	char buf[s.c], c;
	unsigned int x;
	
	for (x = 0; x <= s.c; ++x) {
		drops[x] = -1;
		buf[x] = 0;
	}

	printf("\033[1;1H\033[2J");
	printf("\033[00;36m");

	changemode(1);

	while ( !kbd_break() ) {
		
		x = ranged_rand(0, s.c - 1);
		if (drops[x] == -1)
			drops[x] = 0;
		for (x = 0; x < s.c; ++x) {
			if ( drops[x] >= 0 && drops[x] < 33 ) {
				buf[x] = ranged_rand(33, 126);
				drops[x] += 1;
			}
			else if ( drops[x] == -1 )
				buf[x] = 32;
			else if ( drops[x] == 33 )
				drops[x] = -1;
		}
		puts(buf);
		fflush(stdout);

		usleep(125000);
	}

	c = getchar();
	changemode(0);

	return 0;
}/*}}}*/

// generate random number in range from 0 to upper
int
ranged_rand(int lower, int upper) {
/*{{{*/
	srand(rand());
	unsigned int rr = (rand() % (upper - lower + 1) + lower);
	return rr;
}/*}}}*/

// taken from web to mimic M$ kbhit func
// https://cboard.cprogramming.com/c-programming/63166-kbhit-linux.htm
int
kbd_break(void) {
/*{{{*/
	struct timeval tv;
	fd_set rdfs;

	tv.tv_sec = 0;
	tv.tv_usec = 0;

	FD_ZERO(&rdfs);
	FD_SET (STDIN_FILENO, &rdfs);

	select(STDIN_FILENO+1, &rdfs, NULL, NULL, &tv);

	return FD_ISSET(STDIN_FILENO, &rdfs);
}/*}}}*/

// part of kbd_break... don't really understand them yet
void
changemode(int dir) {
/*{{{*/
	static struct termios oldt, newt;

	if ( dir == 1 ) {
		tcgetattr(STDIN_FILENO, &oldt);
		newt = oldt;
		newt.c_lflag &= ~( ICANON | ECHO );
		tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	}
	else
		tcsetattr(STDIN_FILENO, TCSANOW, &oldt);

	return;
}/*}}}*/
