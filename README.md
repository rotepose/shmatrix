# shmatrix

shmatrix is a screensaver built for use in the terminal, esp. in a tmux driven environment. It is **not** a screenlock.

## Installation

	$ git clone https://gitlab.com/rotepose23/shmatrix
	$ cd shmatrix
	$ gcc shmatrix -o /path/to/my/executables/shmatrix
	$ vim .tmux.conf
 	
		set -g lock-command shmatrix
		set -g lock-after-time 180
		bind L lock-session

## Usage

wait 3 minutes and watch the letters trickle up your screen. Alternatively, C-b L will launch shmatrix immediately.

## Contributing

Hadn't really thought about it but if you like this base and want to make it better, that would be welcome. 

## License

[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)
