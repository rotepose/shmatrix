#ifndef MATRIX_H
#define MATRIX_H

#include <sys/ioctl.h> // ioctl, TIOCGWINSZ
#include <sys/types.h> // for kbd_break and changemode
#include <sys/time.h> // for kbd_break and changemode
#include <termios.h> // for kbd_break and changemode
#include <stdlib.h> // rand
#include <unistd.h> // STDIN_FILENO
#include <stdio.h>

struct scrn {
	int r, c;
};

struct scrn getwin(void);
int rain(struct scrn);
int ranged_rand(int, int);
int kbd_break(void);
void changemode(int);

#endif
